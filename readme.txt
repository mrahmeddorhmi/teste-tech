dans la ligne de commande tapez :

cd C:\wamp64\bin\mysql\mysql5.7.11\bin

puis :
mysql -u root -p -h localhost
pour la connexion avec la base de données

Création de base de données
create database users;

Création de la table user
CREATE TABLE `users`.`user` ( `id` INT NOT NULL AUTO_INCREMENT , `Nom` VARCHAR(10) NOT NULL , `Prenom` VARCHAR(10) NOT NULL , `Email` VARCHAR(30) NOT NULL , `Mot_de_pass` VARCHAR(20) NOT NULL , PRIMARY KEY (`id`), UNIQUE (`Email`));

Pour importer des utilisateures d'un fichier CSV :
use users;
LOAD DATA INFILE 'Chemin vers le fichier' INTO TABLE user
->FIELDS TERMINATED BY ';'
->ENCLOSED BY '"'
->LINES TERMINATED BY '\r\n';

Changer mot de passe à partir de l'email :
UPDATE `user` SET `Mot_de_pass` = 'ton mot de pass' WHERE `user`.`Email` = 'ton email';
