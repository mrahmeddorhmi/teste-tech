<?php

class users extends CI_Model {
   function __construct(){
      parent::__construct();
      $this->load->database();
   }

   function checkuser($email,$password)        //Connexion
   {
     $query = $this->db->query("SELECT * FROM user WHERE Email = ? AND Mot_de_pass = ?",array($email,$password));
     return $query->result_array();
   }


   function AddUser($nom,$prenom,$email,$password){
$this->db->db_debug = FALSE; //disable debugging for queries

$result = $this->db->query("INSERT INTO user VALUES (?,?,?,?,?)",array('',$nom,$prenom,$email,$password)); //run query

return($this->db->error()['code']);

   }

   function getUsers(){
      $query = $this->db->query('SELECT * FROM user ');
      return $query->result_array();
   }

   function GetUserbyId($id)
   {
     $query = $this->db->query("SELECT * FROM user WHERE id = ? ",array($id));
     return $query->result_array();
   }

   function update($id,$nom,$prenom,$email,$password)
   {
$this->db->set('Nom', $nom);
$this->db->set('Prenom', $prenom);
$this->db->set('Email', $email);
$this->db->set('Mot_de_pass', $password);
$this->db->where('id', $id); //which row want to upgrade
$query=$this->db->update('user');  //table name
return $query;
   }

function supprimer($id)
{
$query = $this->db->query('DELETE FROM user WHERE id=?',array($id));
return TRUE;
}


}
