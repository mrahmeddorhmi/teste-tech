<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Teste technique</title>
<link rel="stylesheet" href="<?php echo base_url('bootstrap/css/bootstrap.min.css');?>">
</head>

  <body>
		<div class="container">

		      <form class="form-signin" action="<?=base_url('index.php/welcome/register')?>" method="post">
		        <h2 class="form-signin-heading">Inscription</h2>
		        <label for="inputName" class="sr-only">Le nom :</label>
		        <input type="text" id="inputName" name="Nom" class="form-control" placeholder="Le nom" required>
		        <label for="inputPrenom" class="sr-only">Le prenom :</label>
		        <input type="text" id="inputPrenom" name="Prenom" class="form-control" placeholder="Le prenom" required>
		        <label for="inputEmail" class="sr-only">Email address :</label>
		        <input type="email" id="inputEmail" name="Email" class="form-control" placeholder="Email address" required>
		        <label for="inputPassword" class="sr-only">Password :</label>
		        <input type="password" id="inputPassword" name="Password" class="form-control" placeholder="Password" required>
		        <button class="btn btn-lg btn-success btn-block" type="submit">S'inscrire'</button>
		        <a href="<?=base_url()?>" class="btn btn-lg btn-primary btn-block">Vers L'accueil</a>

		      </form>

		    </div>
  </body>
</html>
