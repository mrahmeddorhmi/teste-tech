<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Teste technique</title>
<link rel="stylesheet" href="<?php echo base_url('bootstrap/css/bootstrap.min.css');?>">
</head>


  <body>
		<?php if ($this->session->userdata('Nom')) { ?>
    Bonjour <?= $this->session->userdata('Nom') ?> vous etes connecté
		<a href="<?=base_url('index.php/welcome/deconnecter')?>">Deconnecter</a>
<?php } else { ?>
	<div class="container">

				<form class="form-signin" action="<?=base_url('index.php/welcome/login')?>" method="post">
					<h2 class="form-signin-heading">Connexion</h2>
					<label for="inputEmail" class="sr-only">Email address</label>
					<input type="email" id="inputEmail" name="email" class="form-control" placeholder="Email address" required>
					<label for="inputPassword" class="sr-only">Password</label>
					<input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required>

					<button class="btn btn-lg btn-primary btn-block" type="submit">Se connecter</button>
					<a href="<?=base_url('index.php/welcome/register')?>" class="btn btn-lg btn-success btn-block">S'inscrire</a>
					<a href="<?=base_url()?>" class="btn btn-lg btn-primary btn-block">Vers L'accueil</a>
				</form>

			</div>
<?php } ?>

  </body>
</html>
