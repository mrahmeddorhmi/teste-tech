

<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Teste technique</title>
<link rel="stylesheet" href="<?php echo base_url('bootstrap/css/bootstrap.min.css');?>">
</head>

  <body>
    <?php if ($this->session->flashdata('category_success')) { ?>
        <div class="alert alert-success"> <?= $this->session->flashdata('category_success') ?> </div>
    <?php } ?>
    <?php if ($this->session->flashdata('category_error')) { ?>
    <div class="alert alert-danger"> <?= $this->session->flashdata('category_error') ?> </div>
<?php } ?>
    <h2>Liste des utilisateurs :</h2>
<div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Nom</th>
                  <th>Prenom</th>
                  <th>Email</th>
                  <th>Mot de pass</th>
                  <th> Actions</th>
                </tr>
              </thead>
              <tbody>


              <?php foreach ($result as $r): ?>
                <tr>
                  <td><?=$r['Nom'] ?></td>
                  <td><?=$r['Prenom'] ?></td>
                  <td><?=$r['Email'] ?></td>
                  <td><?=$r['Mot_de_pass'] ?></td>
                  <td><a href="<?=base_url('index.php/welcome/modifier/'.$r['id'])?>">Modifier</a>&nbsp;&nbsp;<a href="<?=base_url('index.php/welcome/supprimer/'.$r['id'])?>">Supprimer</a></td>
                </tr>
              <?php endforeach; ?>

              </tbody>
            </table>
            <a href="<?=base_url('index.php/welcome/ajouter')?>" class="btn btn-lg btn-success">Ajouter un Utilisateur</a>
            <a href="<?=base_url()?>" class="btn btn-lg btn-primary">Vers L'accueil</a>
          </div>
        </body>
        </html>
