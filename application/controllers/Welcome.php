<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
	}
//Fonction de Connexion
	public function login()
	{
		if($this->input->method()=="post")
		{
			$this->load->model('users');
			$result=$this->users->checkuser($this->input->post('email'),$this->input->post('password'));

		if(!empty($result))  //succés l'utilisateur existe
	{
		$this->load->library('session');
		$this->session->set_userdata(['Nom'=>$result[0]['Nom'],'Prenom'=> $result[0]['Prenom'],'Email'=> $result[0]['Email'],'Password'=> $result[0]['Mot_de_pass']]);

		$this->load->view('Login_page');
	}

		 else {     //Cas d'echec
     	echo "Ce Compte n'existe pas , Veillez s'inscrire Monsieur/Madame";
     }
		}
		else
		{
		 $this->load->view('Login_page');
		}
}


public function deconnecter()
{
	$this->session->sess_destroy();
	$this->load->view('welcome_message');
}



//Fonction d'inscription
	public function register()
	{
		if($this->input->method()=="post") //Enregistrement de l'utilisateur
		{
			$this->load->model('users');
			$result=$this->users->AddUser($this->input->post('Nom'),$this->input->post('Prenom'),$this->input->post('Email'),$this->input->post('Password'));
    //  var_dump($result);
			if($result==1062)
			echo 'ce email existe deja';
		  elseif ($result==0)
			{
				$this->session->set_userdata(['Nom'=>$this->input->post('Nom'),'Prenom'=> $this->input->post('Prenom'),'Email'=> $this->input->post('Email'),'Password'=>$this->input->post('Password')]);
        var_dump($this->session);
				$this->load->view('Login_page');
			}
			 else
				 echo 'une ereur est survenue lors de l\'inscription';
				}
		else
		{
		$this->load->view('Register_page');
	  }
	}

  public function gestion()
	{
		$this->load->model('users');
		$result=$this->users->getUsers();
$this->load->view('Gestion_page',['result'=>$result]);
	}

	public function ajouter()
	{
		if($this->input->method()=="post") //Enregistrement de l'utilisateur
		{
		$this->load->model('users');
		$result=$this->users->AddUser($this->input->post('Nom'),$this->input->post('Prenom'),$this->input->post('Email'),$this->input->post('Password'));
		if($result==1062)
		{
			$this->session->set_flashdata('category_error', 'Ce email existe deja');
      redirect("welcome/gestion");
		}

		elseif ($result==0)
		{
			$this->session->set_flashdata('category_success', 'Utilisateur bien ajouter');
      redirect("welcome/gestion");
		}
		 else
		 $this->session->set_flashdata('category_error', 'une ereur est survenue');
		 redirect("welcome/gestion");
			}
			else {
				$this->load->view('Ajout_page');
			}
}

public function modifier()
{
	if ($this->input->method()=="post") {
		$id = $this->uri->segment(3);
		$this->load->model('users');
		$result=$this->users->update($id,$this->input->post('Nom'),$this->input->post('Prenom'),$this->input->post('Email'),$this->input->post('Password'));
		if($result)
		$this->session->set_flashdata('category_success', 'Utilisateur bien Modifié');
		redirect("welcome/gestion");

	}
	else {
		$id = $this->uri->segment(3);
		$this->load->model('users');
		$result=$this->users->GetUserbyId($id);
		$this->load->view('Ajout_page',['result'=>$result]);
	}

}

public function supprimer()
{
	$id = $this->uri->segment(3);
	$this->load->model('users');
	$result=$this->users->supprimer($id);
	$this->session->set_flashdata('category_success', 'Utilisateur bien Supprimé');
	redirect("welcome/gestion");
}


}
